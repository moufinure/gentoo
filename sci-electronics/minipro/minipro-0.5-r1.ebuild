# Copyright (C) 2021 Pierre Gnaedig <pierre.gnaedig@mailoo.org>
# Distributed under the terms of the GNU General Public License version 2 or later

EAPI=7

inherit flag-o-matic udev

DESCRIPTION="Utility for MiniPro TL866A/TL866/CS programmer"
HOMEPAGE="https://gitlab.com/DavidGriffith/minipro"
SRC_URI="https://gitlab.com/DavidGriffith/minipro/-/archive/${PV}/${P}.tar.gz"

# https://gitlab.com/DavidGriffith/minipro/-/archive/0.4/minipro-0.4.tar.gz

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
    virtual/libusb:1
"

src_install() {
    emake PREFIX=/usr DESTDIR=${D} install
}
