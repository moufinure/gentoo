# Copyright 2020 Pierre Gnaedig
# distributed under the terms of the GNU General Public License v2

EAPI=7
inherit desktop

DESCRIPTION="The Ultimate Doom game."
HOMEPAGE="https://www.idsoftware.com"
KEYWORDS="-* ~amd64 ~x86"
LICENSE="Non-free"
SRC_URI="Doom.wad"
SLOT="0"
RESTRICT="fetch"

DOOMWADPATH=share/doom
DOOMSRCDIR=$DISTDIR

RDEPEND="
	|| (
		games-fps/gzdoom[nonfree(+)]
		games-engines/odamex
		games-fps/doomsday
		games-fps/prboom-plus
	)
"

pkg_nofetch() {
	[[ -z ${A} ]] && return

	elog "The following files cannot be fetched for ${PN}:"
	local x
	for x in ${A}; do
		elog "   ${x}"
	done
}

src_unpack() {
    mkdir -p $S
    for f in $A; do
        cp $(echo $f | tr \[:lower:\]) $S
    done
}

src_install() {
    dodir ${EROOT}/usr/${DOOMWADPATH}/
    doins doom.wad
}
